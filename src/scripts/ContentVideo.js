import mediaelementplayer from 'mediaelement'
import fancybox from '@fancyapps/fancybox'
import * as PIXI from 'pixi.js'
import 'sharer.js'

import { pathsB1 } from './paths'

// Disable scrolling in active popup functions
function disableScrolling(){
    const x = window.scrollX
    const y = window.scrollY
    window.onscroll = function() { window.scrollTo(x, y); }
}
function enableScrolling(){
    window.onscroll = function(){}
}


export default class ContentVideo {

    constructor(siteUrl, ID, name) {
        this.siteUrl = siteUrl
        this.ID = ID
        this.name = name
    }

    init = () => Promise.resolve('ContentVideo init')

    showVideo() {

        let self = this

        const tmplShareBox = `
        <div class="video-share-box">
            <ul class="video-share-box__list">
                <li
                    class="video-share-box__btn video-share-box__btn--vk"
                    data-sharer="vk"
                    data-title='"Письма Победы" - ${this.name}'
                    data-url=${this.siteUrl + '#video-' + this.ID}
                >
                    <i class="fab fa-vk"></i>
                </li>
                <li
                    class="video-share-box__btn video-share-box__btn--fa"
                    data-sharer="facebook"
                    data-title='"Письма Победы" - ${this.name}'
                    data-hashtag="письмапобеды"
                    data-url=${this.siteUrl + '#video-' + this.ID}
                    >
                    <i class="fab fa-facebook-f"></i>
                </li>
                <li
                    class="video-share-box__btn video-share-box__btn--tw"
                    data-sharer="twitter"
                    data-title='"Письма Победы" - ${this.name}'
                    data-hashtags="письмапобеды"
                    data-url=${this.siteUrl + '#video-' + this.ID}
                >
                    <i class="fab fa-twitter"></i>
                </li>
                <li
                    class="video-share-box__btn video-share-box__btn--ok"
                    data-sharer="okru"
                    data-title='"Письма Победы" - ${this.name}'
                    data-url=${this.siteUrl + '#video-' + this.ID}
                >
                    <i class="fab fa-odnoklassniki"></i>
                </li>
            </ul>
        </div>`


        let videoContent = `
        <div class="video-box__player-box">
            ${tmplShareBox}
            <video
                class="video-box__player"
                width="900" height="500">
                    <source
                        src="${pathsB1.video + this.ID + '.1080p.mp4'}"
                        type="video/mp4"
                    />
            </video>
        </div>`

        $.fancybox.open({
            src: videoContent,
            type: 'html',
            hash: false,
            protect: true,
            smallBtn: false,
            hideScrollbar: false,
            buttons : [
              'close'
            ],
            beforeLoad: function( instance, current ) {

                disableScrolling()
            },
            afterLoad: function( instance, current ) {

                let videoTag = $('.video-box__player')
                if (videoTag.mediaelementplayer) {

                    videoTag.mediaelementplayer({
                        hideVideoControlsOnLoad: true,
                        success: function(mediaElement, originalNode, instance) {

                        }
                    })
                }

                history.pushState(null, '', `#video-${self.ID}`)

                window.Sharer.init()
            },
            beforeClose: function( instance, current ) {

                history.pushState(null, '', '/')

                enableScrolling()
            }
        })

    }
}
