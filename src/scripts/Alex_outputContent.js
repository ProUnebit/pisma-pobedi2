// import '../common_libraries/jquery-global'
import 'jquery-backstretch'
const ScrollReveal = require('scrollreveal').default

import { pathsB1 } from './paths'
import ContentVideo from './ContentVideo'
import ContentPopup from './ContentPopup'

// Preload images
let images = new Array()
function preload() {
    for (var i = 0; i < arguments.length; i++) {
        images[i] = new Image()
        images[i].src = arguments[i]
    }
}

preload(
    "/participants-page/assets/bg-red-min.png",
    "/participants-page/assets/35.png",
    "/participants-page/assets/46.png",
    "/participants-page/assets/2506.png",
    "/participants-page/assets/bg-title.svg"
)

function DOMIsReady() {

    // Fixed main bg
    // $.backstretch(
    //     "https://github.com/ProUnebit/Reminder_ToMe_Reminder/blob/master/src/assets/img/bg-red-min.png?raw=true",
    //     {transitionDuration: 400}
    // )
    $.backstretch(
        "/participants-page/assets/bg-red-min.png",
        {transitionDuration: 400}
    )

    // Init ScrollReveals titles
    ScrollReveal({
        reset: true,
        origin: 'right',
        distance: '20px',
        duration: 1800,
        interval: 0,
    }).reveal('.page-title-box')

    if (document.getElementById('video-page')) {

        document.querySelectorAll('.video-box__wrapper-for-item').forEach(videoCard => {

            const videoPopup = new ContentVideo('http://pismapobedy.ru/', videoCard.dataset.id, videoCard.dataset.name)

            videoCard.addEventListener('click', () => {
                // Init detail video popup for each video-card
                videoPopup.init()
                    .then(() => videoPopup.showVideo())
            })

            // Open popup with hash
            if (window.location.href.indexOf(`#video-${videoCard.dataset.id}`) != -1) {

                console.log('open participant-popup with hash')

                if (`#video-${videoCard.dataset.id}` == window.location.hash) {
                    videoPopup.init()
                        .then(() => videoPopup.showVideo())
                }
            }
        })
    }

    if (document.getElementById('participants-page')) {

        const partiContainer = document.querySelector('.participants-container')
        // Preloader, before fetch
        partiContainer.innerHTML = `
        <div class="participants-container__preloader">
            <svg version="1.1" id="L3" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
            viewBox="0 0 100 100" enable-background="new 0 0 0 0" xml:space="preserve">
                <circle fill="none" stroke="rgba(250, 234, 210, 1)" stroke-width="4" cx="50" cy="50" r="44" style="opacity:0.45;" />
                <circle fill="rgba(75, 15, 16, 1)" stroke="rgba(158, 138, 117, 1)" stroke-width="2" cx="8" cy="54" r="6">
                    <animateTransform
                      attributeName="transform"
                      dur="1.75s"
                      type="rotate"
                      from="0 50 48"
                      to="360 50 52"
                      repeatCount="indefinite" />
                </circle>
            </svg>
        </div>`
        // Get Data
        fetch('http://pismapobedy.ru/heroes/frontend/web/site/get')
            .then(res => res.json())
            .then(participantsList => new Promise((res, rej) => {

                setTimeout(() => {
                    // Clear container after get data
                    partiContainer.innerHTML = ''

                    // Add illusive cards for grid system
                    const illusiveCard1 = document.createElement('div')
                    illusiveCard1.className = 'participants-container__wrapper-for-item participants-container__illusive-item-1'
                    illusiveCard1.innerHTML = `
                    <div>
                        <p>Письма</p>
                    </div>`
                    partiContainer.appendChild(illusiveCard1)
                    const illusiveCard2 = document.createElement('div')
                    illusiveCard2.className = 'participants-container__wrapper-for-item participants-container__illusive-item-2'
                    illusiveCard2.innerHTML = `
                    <div>
                        <p>Победы</p>
                    </div>`
                    partiContainer.appendChild(illusiveCard2)

                    // Draw participants list
                    participantsList.forEach((participantItem, index) => {

                        const { id, fio, titr, city, img, video, description } = participantItem

                        const participantCard = document.createElement('div')
                        participantCard.className = 'participants-container__wrapper-for-item for-reveal'
                        participantCard.innerHTML = `
                        <div
                            class='participants-container__item'
                            data-id='${id}'
                            data-fio='${fio}'
                            data-titr='${titr}'
                            data-city='${city}'
                            data-img='${img}'
                            data-video='${video}'
                            data-description='${description}'
                            data-src='${pathsB1.img + (img[0] ? img[0] : video[0] + .5)}.jpg'
                        >
                            <div
                                class="participants-container__img-box"
                                style="background: url(${img[0] || video[0] ? (pathsB1.img + (img[0] ? img[0] : video[0] + .5)) : (pathsB1.img + '1217677.5.440x230')}.440x230.jpg) no-repeat; background-size: cover; background-position: center"
                            >
                                <div class="participants-container__titr-box">
                                    <h4>${titr ? titr : fio}</h4>
                                </div>
                            </div>

                        </div>`
                        // Add drow
                        partiContainer.appendChild(participantCard)

                        // Init detail popup for each
                        const popup = new ContentPopup(participantItem, 'http://pismapobedy.ru/')

                        participantCard.addEventListener('click', () => {
                            popup.initPopup()
                                .then(() => popup.showContentFromData(participantItem))
                        })
                        // Open popup with hash
                        if (window.location.href.indexOf(`#participant-${id}`) != -1) {

                            console.log('open participant-popup with hash')

                            if (`#participant-${id}` == window.location.hash) {
                                popup.initPopup().then(() => popup.showContentFromData(participantItem))
                            }
                        }
                    })
                    // Response
                    res()
                }, 1000)
            }))
            .then(() => {
                // Init ScrollReveals cards
                ScrollReveal({
                    reset: false,
                    origin: 'top',
                    distance: '20px',
                    duration: 1500,
                    interval: 5000,
                    viewFactor: 0.35
                }).reveal('.for-reveal')
            })
    }
}

document.addEventListener('DOMContentLoaded', DOMIsReady)
