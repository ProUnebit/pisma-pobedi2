import mediaelementplayer from 'mediaelement'
import fancybox from '@fancyapps/fancybox'
import Glide from '@glidejs/glide'
import PerfectScrollbar from 'perfect-scrollbar'
import imagesLoaded from 'imagesloaded'
 // import { TweenMax, TimelineMax } from 'gsap/TweenMax'
import 'sharer.js'

import { pathsB1 } from './paths'

if (!Element.prototype.remove) {
  Element.prototype.remove = function remove() {
    if (this.parentNode) {
      this.parentNode.removeChild(this)
    }
  };
}

// Disable scrolling in active popup functions
function disableScrolling(){
    const x = window.scrollX
    const y = window.scrollY
    window.onscroll = function() { window.scrollTo(x, y); }
}
function enableScrolling(){
    window.onscroll = function(){}
}

export default class ContentPopup {

    constructor(participantObj, siteUrl) {
        this.participantObj = participantObj
        this.siteUrl = siteUrl
        this.siteHash = window.location.pathname
    }

    initPopup() {

        return Promise.resolve('ContentPopup init')
    }

    showContentFromData(participantObj) {

        const { id, fio, titr, city, img, video, description } = participantObj

        const tmplPreloader = `
        <div class="participant-detail-popup__img-preloader">
            <svg version="1.1" id="L3" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
            viewBox="0 0 100 100" enable-background="new 0 0 0 0" xml:space="preserve">
                <circle fill="none" stroke="rgba(250, 234, 210, 1)" stroke-width="4" cx="50" cy="50" r="44" style="opacity:0.45;" />
                <circle fill="rgba(75, 15, 16, 1)" stroke="rgba(158, 138, 117, 1)" stroke-width="2" cx="8" cy="54" r="6">
                    <animateTransform
                      attributeName="transform"
                      dur="1.75s"
                      type="rotate"
                      from="0 50 48"
                      to="360 50 52"
                      repeatCount="indefinite" />
                </circle>
            </svg>
        </div>`

        const tmplShareBox = `
        <div class="caption-share-box">
            <span>Поделиться</span>
            <ul class="caption-share-box__list">
                <li
                    class="caption-share-box__btn caption-share-box__btn--vk"
                    data-sharer="vk"
                    data-title='"Письма Победы" - ${titr ? titr : fio}'
                    data-url=${this.siteUrl + '#participant-' + id}
                >
                    <i class="fab fa-vk"></i>
                </li>
                <li
                    class="caption-share-box__btn caption-share-box__btn--fa"
                    data-sharer="facebook"
                    data-title='"Письма Победы" - ${titr ? titr : fio}'
                    data-hashtag="письмапобеды"
                    data-url=${this.siteUrl + '#participant-' + id}
                    >
                    <i class="fab fa-facebook-f"></i>
                </li>
                <li
                    class="caption-share-box__btn caption-share-box__btn--tw"
                    data-sharer="twitter"
                    data-title='"Письма Победы" - ${titr ? titr : fio}'
                    data-hashtags="письмапобеды"
                    data-url=${this.siteUrl + '#participant-' + id}
                >
                    <i class="fab fa-twitter"></i>
                </li>
                <li
                    class="caption-share-box__btn caption-share-box__btn--ok"
                    data-sharer="okru"
                    data-title='"Письма Победы" - ${titr ? titr : fio}'
                    data-url=${this.siteUrl + '#participant-' + id}
                >
                    <i class="fab fa-odnoklassniki"></i>
                </li>
            </ul>
        </div>`

        const tmplContent = `
        <div class="participant-detail-popup">
            <div class="participant-detail-popup__content">
                <div class="participant-detail-popup__slider-box">
                    <div class="participant-detail-popup__wrapper-for-slider">
                        ${ tmplShareBox }
                        ${ (video.length + img.length) > 1 ?
                            `<span class="participant-detail-popup__current-index">
                                <i>1</i>/<b>${video.length + img.length}</b>
                            </span>`
                            :
                            ''
                        }
                        <div class="participant-detail-popup__slider glide">
                            ${ video.length > 1 || img.length > 1 || (video.length + img.length) > 1 ?
                                `<div class="glide__arrows" data-glide-el="controls">
                                    <button class="glide__arrow glide__arrow--left" data-glide-dir="<">
                                        <i class="fas fa-chevron-left"></i>
                                    </button>
                                    <button class="glide__arrow glide__arrow--right" data-glide-dir=">">
                                        <i class="fas fa-chevron-right"></i>
                                    </button>
                                </div>`
                                :
                                ''
                            }
                            <div data-glide-el="track" class="glide__track">
                                <ul class="glide__slides">
                                    ${ video.length > 0 ?
                                        video.map(eachVideo => {
                                            const videoTag = `
                                            <video
                                                class="participant-detail-popup__video-tag"
                                                width="360" height="360">
                                                    <source
                                                        src="${pathsB1.video + eachVideo + '.m3u8'}"
                                                        type="application/x-mpegURL"
                                                    />
                                                    <source
                                                        src="${pathsB1.video + eachVideo + '.360p.mp4'}"
                                                        type="video/mp4"
                                                    />
                                            </video>`

                                            return (
                                                `<li class="participant-detail-popup__slide glide__slide">
                                                    ${videoTag}
                                                </li>`
                                            )
                                        })
                                        :
                                        ''
                                    }
                                    ${ img.length > 0 ?
                                        img.map(eachImg => {
                                            const imgTag = `
                                            <div
                                                class="participant-detail-popup__wrapper-img-tag">
                                                <span
                                                    class="participant-detail-popup__zoom-img-btn"
                                                    data-current-img="${pathsB1.img + eachImg + '.jpg'}">
                                                    <i class="far fa-eye"></i>
                                                </span>
                                                ${tmplPreloader}
                                                <img
                                                    class="participant-detail-popup__img-tag"
                                                    src="${pathsB1.img + eachImg + '.560x360' + '.jpg'}"
                                                    alt=""
                                                />
                                            </div>`

                                            return (
                                                `<li class="participant-detail-popup__slide glide__slide">
                                                    ${imgTag}
                                                </li>`
                                            )
                                        })
                                        :
                                        ''
                                    }
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="participant-detail-popup__info-box">
                    <div class="participant-detail-popup__titles">
                        <h5>${titr ? titr : fio}</h5>
                        <h5>г. ${city ? city : ''}</h5>
                    </div>
                    <div class="participant-detail-popup__description">
                        <p>${description}</p>
                    </div>
                </div>
            </div>
        </div>`

        const partiDetailPopup = $.fancybox.open(
            {
                src: tmplContent,
                type: 'html',
                smallBtn: (window.innerWidth || document.documentElement.clientWidth ) < 760 ? true : false,
                hash: false,
                protect: true,
                hideScrollbar: false,
                trapFocus: true,
                buttons : [
                  'close'
                ],
                beforeLoad: function( instance, current ) {

                    disableScrolling()
                },
        		afterLoad: function( instance, current ) {

                    // Remove ',' after LI slide, array map legacy
                    const slidesLI = document.querySelectorAll('.participant-detail-popup__slide')
                    slidesLI.forEach(slideLI => {
                        if (slideLI.nextSibling.textContent === ',' && slideLI.nextSibling.nodeType === 3) {
                            slideLI.nextSibling.remove()
                        }
                    })

                    // Init video player
                    let videoTag = $('.participant-detail-popup__video-tag')
                    if (videoTag.mediaelementplayer) {

                        videoTag.mediaelementplayer({
                            hideVideoControlsOnLoad: true,
                            success: function(mediaElement, originalNode, instance) {

                                mediaElement.addEventListener('canplay', () => {
                                     mediaElement.pause()
                                 }, false)
                            }
                        })
                    }

                    // Init slider
                    const popupInsideSlider = new Glide('.glide', {
                        type: 'slider',
                        startAt: 0,
                        perView: 1,
                        gap: 20,
                        dragThreshold: 110,
                        perTouch: 1,
                        animationDuration: 900,
                        rewindDuration: 1600,
                    })
                    popupInsideSlider.mount()
                    // Draw current index in slider
                    if ((video.length + img.length) > 1) {
                        let currentIndex = document.querySelector('.participant-detail-popup__current-index i')
                        popupInsideSlider.on(['move'], function () {
                            setTimeout(() => {
                                currentIndex.textContent = popupInsideSlider.index + 1
                            }, 260)
                        })
                    }

                    // Init perfect scroll
                    const ps = new PerfectScrollbar(
                        '.participant-detail-popup__description P',
                        {
                             wheelSpeed: 0.25,
                             maxScrollbarLength: 125
                        }
                    )

                    // Images load (in popup)
                    const imgLoad = imagesLoaded(document.querySelectorAll('.participant-detail-popup__img-tag'))
                    imgLoad.on('progress', (instance, image) => {
                        image.isLoaded ? image.img.parentElement.children[1].remove() : ''
                    });

                    // Init detail letters popup img
                    document.querySelectorAll('.participant-detail-popup__wrapper-img-tag').forEach(wrapperImg => {
                        wrapperImg.querySelectorAll('.participant-detail-popup__zoom-img-btn').forEach(zoomImgBtn => {
                            zoomImgBtn.addEventListener('click', () => {
                                // e.preventDefault()
                                $.fancybox.open({
                                    src: zoomImgBtn.dataset.currentImg,
                                    baseClass: 'zoom-img-container',
                                    hash: false,
                                    protect: true,
                                    smallBtn: true,
                                    image: {
                                        preload: true
                                    },
                                })
                            })
                        })
                    })

                    // Change history
                    history.pushState(null, '', `#participant-${id}`)

                    // Init caption share boxmodel
                    window.Sharer.init()
		        },
                beforeClose: function( instance, current ) {
                    // Comeback history
                    history.pushState(null, '', '/')

                    enableScrolling()
                }
            }
        )
    }
}
